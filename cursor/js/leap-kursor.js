"use strict";
/*jslint browser: true, devel: true */
/*global $, jQuery, Leap */

/* leap color */

// vars + cached selectors
var nyan = $('#kursor'); //document.getElementById('kursor');
var sceneHeight = 600;
var sky = $('#sky');
var makeStars;
var tunez = document.getElementById('tunez'); //$('#tunez');

/* kursor object */
var kursor = {
    x: 400,
    y: -300,

    update: function () {
        nyan.removeClass();
        nyan.css('top', this.y + 'px');
        nyan.css('left', this.x + 'px');
    },

    autoPilot: function () {
        nyan.addClass('float').removeAttr('style');
        kursor.x = 400;
        kursor.y = -300;
    }

};

/* audio */

tunez.volume = '0.5';


/* stars */

var starId = 0;

// array of stars for removal
var stars = [];


// class
function Star(id) {

    this.id = id;
    this.top = Math.floor(Math.random() * 600);

    // star html
    var starHtml = '<div class="star" id="' + this.id + '" style="top: ' + this.top + 'px"></div>';
    sky.append(starHtml);
    $('#s' + starId).addClass('go');

    starId += 1;
}

var starMaker = setInterval(function () {

    if (makeStars) {
        // create new star
        var newStar = new Star('s' + (starId + 1));

        // save star in array
        stars.push(newStar);
        
        // tidy up past stars
        if (stars.length > 20) {
            $('#' + stars[0].id).remove();
            stars[0] = null;
            stars.splice(0, 1);

        }    console.log(stars);
    }

}, 200);



/* leap */
var pausedFrame = null;
var latestFrame = null;



// init and cfg
var controller = new Leap.Controller({
        enableGestures: true
    });

// leap loop
controller.loop(function (frame) {
    latestFrame = frame;

    if (typeof frame.hands[0] === 'undefined') {

        kursor.autoPilot();
        tunez.pause();

    } else {

        // calc x pos + bound
        kursor.x = Math.floor(frame.hands[0].palmPosition[0] * 4);
        if (kursor.x < 10) {
            kursor.x = 10;
        }
        if (kursor.x > 758) {
            kursor.x = 758;
        }

        // calc y pos + bound
        kursor.y = -(Math.floor(frame.hands[0].palmPosition[1]) * 3 - 150);
        if (kursor.y > -42) {
            kursor.y = -42;
        }
        if (kursor.y < -590) {
            kursor.y = -590;
        }

        // update nyan pos
        kursor.update();

        // turn on music
        tunez.play();        
    }


    $('#xpos').html(kursor.x);
    $('#ypos').html(kursor.y);

    //console.log('x: ' + kursor.x + ' y: ' + kursor.y);

});



/* buttons and listners */

$('.r').on('change', function () {

    if ($('#yes')[0].checked) {
        makeStars = true;
        starMaker;
    } else {
        makeStars = false;
    }

});










