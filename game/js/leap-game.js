"use strict";
/*jslint browser: true, devel: true */
/*global $, jQuery, Leap */

/* leap color */

// vars + cached selectors
var ship = $('#kursor'); //document.getElementById('kursor');
var sceneHeight = 600;
var sky = $('#sky');
var makerocks;
var tunez = document.getElementById('tunez'); //$('#tunez');

/* kursor object */
var kursor = {
    x: 400,
    y: -300,

    update: function () {
        ship.removeClass();
        ship.css('top', this.y + 'px');
        ship.css('left', this.x + 'px');
    },

    autoPilot: function () {
        ship.addClass('float').removeAttr('style');
        kursor.x = 400;
        kursor.y = -300;
    }

};

/* audio */

tunez.volume = '0.5';


/* rocks */

var rockId = 0;

// array of rocks for removal
var rocks = [];


// class
function Rock(id) {

    this.id = id;
    this.top = 300; // Math.floor(Math.random() * 600);
    

    this.crashCheck = function() {

        var pos = $('#' + this.id).position();
       //return 'returned this';
        // console.log(pos.left);

        // check for Y axis crash
        if (this.top > -kursor.y && this.top < (-kursor.y + 25)) {
console.log('y crash');
            // check for X axis crash
            if (pos.x == kursor.x) {
                console.log('x crash');
            }
        }



        // if(pos.left){
        //     //return crash;
        //     console.log('crash');
        //  } //else {
        //  // console.log(pos.left);
        // // }



    }

    // rock html
    var rockHtml = '<div class="rock" id="' + this.id + '" style="top: ' + this.top + 'px"></div>';
    sky.append(rockHtml);
    $('#s' + rockId).addClass('go');

    rockId += 1;
}

// rock generator
var rockMaker = setInterval(function () {

    //if (makerocks) {
        // create new rock
        var newrock = new Rock('s' + (rockId + 1));

        // save rock in array
        rocks.push(newrock);
        
        // tidy up past rocks
        if (rocks.length > 4) {
            $('#' + rocks[0].id).remove();
            rocks[0] = null;
            rocks.splice(0, 1);

        }   // console.log(rocks);
   // }

}, 1500);

// rock crash check
setInterval(function () {
    // update rock positions
    for (var i = rocks.length - 1; i >= 0; i--) {
        rocks[i].crashCheck();
    };
}, 100);



/* leap */
var pausedFrame = null;
var latestFrame = null;



// init and cfg
var controller = new Leap.Controller({
        enableGestures: true
    });

// leap loop
controller.loop(function (frame) {
    latestFrame = frame;


    // if no hand detected
    if (typeof frame.hands[0] === 'undefined') {

        kursor.autoPilot();
        tunez.pause();

    } else {

        // calc x pos + bound
        kursor.x = Math.floor(frame.hands[0].palmPosition[0] * 4);
        if (kursor.x < 10) {
            kursor.x = 10;
        }
        if (kursor.x > 758) {
            kursor.x = 758;
        }

        // calc y pos + bound
        kursor.y = -(Math.floor(frame.hands[0].palmPosition[1]) * 3 - 150);
        if (kursor.y > -42) {
            kursor.y = -42;
        }
        if (kursor.y < -590) {
            kursor.y = -590;
        }

        // update ship pos
        kursor.update();

        // turn on music
        tunez.play();        
    }


    $('#xpos').html(kursor.x);
    $('#ypos').html(kursor.y);

    //console.log('x: ' + kursor.x + ' y: ' + kursor.y);

});



/* buttons and listners */

$('.r').on('change', function () {

    if ($('#yes')[0].checked) {
        makerocks = true;
        rockMaker;
    } else {
        makerocks = false;
    }

});










