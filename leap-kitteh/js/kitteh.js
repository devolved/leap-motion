/* leap kitteh - touch image gallery

/* 3rd party includes */

// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
 
// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
 
// MIT license
 
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
 
    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());


// cached selectors
var body = $('body');
var frame = $('#frame');
var kitteh = $('.kitteh');
var filmstrip  = $('#film');

// world
var scene = {
	frame:
	{
		width: 0
	},
	film:
	{
		width: 0
	},
	reportCount: 0,
	reportWidth: 200,
	reportSpacing: 0,
	speed: 1
}

window.addEventListener("resize", function(){
	scene.frame.width = +$('#frame').css('width').slice(0,-2);
}, true);



/*  main array and object class */

var film = {
	reports: [],
	images: [],
	animation:
	{
		type: 'loop',
		speed: 10,
		direction: 'left',
		pauseOnSelect: false
	}
}


	






// touch functions
var dragStart = 0;
var filmX = 0;
var momentum = 0;
var eventDirection;
var speed = 0;


$('#film').css({'transform': 'translateX(' + (event.gesture.touches[0].pageX - dragStart) + 'px)'});


var friction = 5;

function slowDown(event){


	if(momentum > 0){

		if(eventDirection === 'right'){

			filmX = filmX + speed;
		}
		if(eventDirection === 'left'){
			filmX = filmX - speed;			
		}
		momentum = momentum - friction;
		$('#film').css({'transform': 'translateX(' + filmX + 'px)'});
		requestAnimationFrame(slowDown);

	}
	else{
		//cancelAnimationFrame();
	}

	//console.log('slow');
}

function loadReport(target){

	var id = +$(target).attr('id').substring(1) - 1;
	var imgLeft = (scene.frame.width - film.images[id].width) / 2;
	var imgTop = (film.images[id].height - 250) / 2;

	console.log(imgLeft);

	$('#focus').html('<img src="' + film.images[id].src + '">').css({'left': imgLeft + 'px', 'top': -imgTop + 'px'}).addClass('loaded');

}

function closeReport(target){
	$('#focus').removeClass('loaded');
}


// kitteh class
function Kitteh(id){

	this.id = id;
	this.content = {};
	this.content.heading = 'heading';
	this.content.words = 'text ' + this.id;
	this.color = generateRGB(20);

	//var newReport = '<div id="' + this.id + '" class="kitteh" style="left: ' + (scene.reportCount * (scene.reportWidth + scene.reportSpacing)) + 'px; background: rgb(' + this.color + ')"><h3>' + this.content.heading + '</h3><p>' + this.content.words + '</p></div>';
	var newReport = '<div id="' + this.id + '" class="kitteh" style="background: url(./images/' + (scene.reportCount + 1) + '.jpg)"></div>';

	film.reports.push(newReport);

	scene.reportCount ++;

	//update film width
	scene.film.width = scene.reportCount * (scene.reportWidth + scene.reportSpacing);
	$('#film').css('width', scene.film.width + 'px').append(newReport);
}


function generateRGB(){
	var rgbColors = [];
	for (var i=0; i < 3; i++) 
	{            
	   var rgbVal = Math.floor(Math.random() * 255);
	   rgbColors.push(rgbVal);
	}
	return rgbColors.toString();
}

	

$(document).ready(function () {

// set frame width
scene.frame.width = +$('#frame').css('width').slice(0,-2);


$('#new').on('click', function(){

	var newReport = new Kitteh('r' + (scene.reportCount + 1));

});

// end doc ready
});


$(window).load(function(){

	// load images to array
	$('#img-dump img').each(function(index) {

		var newImg = {};
		newImg.src = $(this).attr('src');
		newImg.width = $(this).width();
		newImg.height = $(this).height();	

		film.images[index] = newImg;

	console.log(film.images[index]);

	});

});

// prevent over scroll
$(document).on(
	'touchmove',
	function(e){
		e.preventDefault();
	}
);







