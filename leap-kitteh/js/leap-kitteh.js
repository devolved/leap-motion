/* leap kitteh - touch image gallery

/* 3rd party includes */

// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
 
// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
 
// MIT license
 
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
 
    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());


// cached selectors
var body = $('body');
var lk = $('#leap-kitteh');
var kittehs = $('#kittehs');
var litter = $('#litter-box');

/* leap */
var pausedFrame = null;
var latestFrame = null;
var pos = 0;

// init and cfg
var controller = new Leap.Controller({
                                        enableGestures: true
                                    });
//var paw = new Leap.Hand();

var dump;

// leap loop
controller.loop(function(frame) {
    latestFrame = frame;
    
  




    // gesture control
    if (frame.gestures.length > 0) {

        for (var i = 0; i < frame.gestures.length; i++) {
            var gesture = frame.gestures[i];
            var speed = gesture.speed / 60;

            // detect swipe   
            if (gesture.direction[0] > gesture.direction[2]) {
                pos += speed;
                $('#kittehs').css({'transform': 'translateX(' + pos + 'px)'});
            }
            else {
                pos -= speed;
                $('#kittehs').css({'transform': 'translateX(' + pos + 'px)'});                    
            }
        }
    }

    // pick a random kitteh
    if (frame.fingers.length === 2) {
        pos += (frame.hands[0].palmPosition[0] / 10);
        $('#kittehs').css({'transform': 'translateX(' + pos + 'px)'});
    }
    else {
            if (frame.hands[0].palmPosition[0] > 0) {
                if(!lightbox){
                    pickKitty();
                }
            }    
            if (frame.hands[0].palmPosition[0] < 0) {
                killKitty();
            }
    }

    //$('#x').text('x ' + frame.hands[0].palmPosition[0]);
    //$('#y').text('y ' + frame.hands[0].palmPosition[1]);
    //$('#z').text('z ' + frame.hands[0].palmPosition[2]);
    
    //dump = frame;

});



/* misc */

function generateRGB(){
    var rgbColors = [];
    for (var i=0; i < 3; i++) 
    {            
       var rgbVal = Math.floor(Math.random() * 255);
       rgbColors.push(rgbVal);
    }
    return rgbColors.toString();
}


controller.on('Leap.SwipeGesture', function() {
    console.log('controller.gestures');
});

$(document).on('click', '#dump', function(){
        console.log(dump);
});



/* lightbox */

lightbox = false;

function pickKitty() {
    lightbox = true;

    var rnd = Math.ceil(Math.random()*7);
    $('#litter-box').html('<img src="./images/' + rnd + '.jpg">').addClass('show');
    $('body').css('background', 'rgb(' + generateRGB() + ')');

}

function killKitty() {
    $('#litter-box').removeClass('show');
    lightbox = false;
}











