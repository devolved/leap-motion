# LEAP MOTION

## Dependencies

+leap.js
+ jQuery


# LEAP KURSOR


    +      o     +              o   
        +             o     +       +
    o          +
        o  +           +        +
    +        o     o       +        o
    -_-_-_-_-_-_-_,------,      o 
    _-_-_-_-_-_-_-|   /\_/\  
    -_-_-_-_-_-_-~|__( ^ .^)  +     +  
    _-_-_-_-_-_-_-""  ""      
    +      o         o   +       o
        +         +
    o        o         o      o     +
        o           +
    +      +     o        o      + 



# LEAP COLOR

+ Change background colour based on palm input


# LEAP MOTION KITTEH

+ Leap enabled image gallery test


# LEAP GAME TEST

+ Collision detection


