/* leap color */


// cached selectors
var body = $('body');

/* leap */
var pausedFrame = null;
var latestFrame = null;


// init and cfg
var controller = new Leap.Controller({
                                        enableGestures: true
                                    });

// leap loop
controller.loop(function(frame) {
    latestFrame = frame;

    var r = Math.floor(frame.hands[0].palmPosition[0]);
    var g = Math.floor(frame.hands[0].palmPosition[1]);
    var b = Math.floor(frame.hands[0].palmPosition[2]);

    $('section').css('background', 'rgb(' + r + ',' + g + ',' + b + ')');

});













